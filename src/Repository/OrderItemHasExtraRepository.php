<?php

namespace App\Repository;

use App\Entity\OrderItemHasExtra;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderItemHasExtra|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderItemHasExtra|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderItemHasExtra[]    findAll()
 * @method OrderItemHasExtra[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderItemHasExtraRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderItemHasExtra::class);
    }

    // /**
    //  * @return OrderItemHasExtra[] Returns an array of OrderItemHasExtra objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderItemHasExtra
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
