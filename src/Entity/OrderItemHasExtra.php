<?php

namespace App\Entity;

use App\Repository\OrderItemHasExtraRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderItemHasExtraRepository::class)
 */
class OrderItemHasExtra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=OrderItems::class, inversedBy="orderItemHasExtras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $orderItems;

    /**
     * @ORM\ManyToOne(targetEntity=Extras::class, inversedBy="orderItemHasExtras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $extras;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderItems(): ?OrderItems
    {
        return $this->orderItems;
    }

    public function setOrderItems(?OrderItems $orderItems): self
    {
        $this->orderItems = $orderItems;

        return $this;
    }

    public function getExtras(): ?Extras
    {
        return $this->extras;
    }

    public function setExtras(?Extras $extras): self
    {
        $this->extras = $extras;

        return $this;
    }
}
