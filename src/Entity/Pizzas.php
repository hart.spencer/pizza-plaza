<?php

namespace App\Entity;

use App\Repository\PizzasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PizzasRepository::class)
 */
class Pizzas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=OrderItems::class, mappedBy="pizzas")
     */
    private $orderItems;

    /**
     * @ORM\OneToMany(targetEntity=PizzaHasExtra::class, mappedBy="pizzas", orphanRemoval=true)
     */
    private $pizzaHasExtras;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
        $this->pizzaHasExtras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|OrderItems[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItems $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setPizzas($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItems $orderItem): self
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getPizzas() === $this) {
                $orderItem->setPizzas(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PizzaHasExtra[]
     */
    public function getPizzaHasExtras(): Collection
    {
        return $this->pizzaHasExtras;
    }

    public function addPizzaHasExtra(PizzaHasExtra $pizzaHasExtra): self
    {
        if (!$this->pizzaHasExtras->contains($pizzaHasExtra)) {
            $this->pizzaHasExtras[] = $pizzaHasExtra;
            $pizzaHasExtra->setPizzas($this);
        }

        return $this;
    }

    public function removePizzaHasExtra(PizzaHasExtra $pizzaHasExtra): self
    {
        if ($this->pizzaHasExtras->removeElement($pizzaHasExtra)) {
            // set the owning side to null (unless already changed)
            if ($pizzaHasExtra->getPizzas() === $this) {
                $pizzaHasExtra->setPizzas(null);
            }
        }

        return $this;
    }
}
