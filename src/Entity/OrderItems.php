<?php

namespace App\Entity;

use App\Repository\OrderItemsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderItemsRepository::class)
 */
class OrderItems
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity=Pizzas::class, inversedBy="orderItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pizzas;

    /**
     * @ORM\OneToMany(targetEntity=OrderItemHasExtra::class, mappedBy="orderItems", orphanRemoval=true)
     */
    private $orderItemHasExtra;

    public function __construct()
    {
        $this->orderItemHasExtras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getPizzas(): ?Pizzas
    {
        return $this->pizzas;
    }

    public function setPizzas(?Pizzas $pizzas): self
    {
        $this->pizzas = $pizzas;

        return $this;
    }

    /**
     * @return Collection|OrderItemHasExtra[]
     */
    public function getOrderItemHasExtra(): Collection
    {
        return $this->orderItemHasExtra;
    }

    public function addOrderItemHasExtra(OrderItemHasExtra $orderItemHasExtra): self
    {
        if (!$this->orderItemHasExtra->contains($orderItemHasExtra)) {
            $this->orderItemHasExtra[] = $orderItemHasExtra;
            $orderItemHasExtra->setOrderItems($this);
        }

        return $this;
    }

    public function removeOrderItemHasExtra(OrderItemHasExtra $orderItemHasExtra): self
    {
        if ($this->orderItemHasExtra->removeElement($orderItemHasExtra)) {
            // set the owning side to null (unless already changed)
            if ($orderItemHasExtra->getOrderItems() === $this) {
                $orderItemHasExtra->setOrderItems(null);
            }
        }

        return $this;
    }
}
