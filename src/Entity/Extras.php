<?php

namespace App\Entity;

use App\Repository\ExtrasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExtrasRepository::class)
 */
class Extras
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isChoosable;

    /**
     * @ORM\OneToMany(targetEntity=PizzaHasExtra::class, mappedBy="extras", orphanRemoval=true)
     */
    private $pizzaHasExtras;

    /**
     * @ORM\OneToMany(targetEntity=OrderItemHasExtra::class, mappedBy="extras", orphanRemoval=true)
     */
    private $orderItemHasExtras;

    public function __construct()
    {
        $this->pizzaHasExtras = new ArrayCollection();
        $this->orderItemHasExtras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIsChoosable(): ?bool
    {
        return $this->isChoosable;
    }

    public function setIsChoosable(bool $isChoosable): self
    {
        $this->isChoosable = $isChoosable;

        return $this;
    }

    /**
     * @return Collection|PizzaHasExtra[]
     */
    public function getPizzaHasExtras(): Collection
    {
        return $this->pizzaHasExtras;
    }

    public function addPizzaHasExtra(PizzaHasExtra $pizzaHasExtra): self
    {
        if (!$this->pizzaHasExtras->contains($pizzaHasExtra)) {
            $this->pizzaHasExtras[] = $pizzaHasExtra;
            $pizzaHasExtra->setExtras($this);
        }

        return $this;
    }

    public function removePizzaHasExtra(PizzaHasExtra $pizzaHasExtra): self
    {
        if ($this->pizzaHasExtras->removeElement($pizzaHasExtra)) {
            // set the owning side to null (unless already changed)
            if ($pizzaHasExtra->getExtras() === $this) {
                $pizzaHasExtra->setExtras(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrderItemHasExtra[]
     */
    public function getOrderItemHasExtras(): Collection
    {
        return $this->orderItemHasExtras;
    }

    public function addOrderItemHasExtra(OrderItemHasExtra $orderItemHasExtra): self
    {
        if (!$this->orderItemHasExtras->contains($orderItemHasExtra)) {
            $this->orderItemHasExtras[] = $orderItemHasExtra;
            $orderItemHasExtra->setExtras($this);
        }

        return $this;
    }

    public function removeOrderItemHasExtra(OrderItemHasExtra $orderItemHasExtra): self
    {
        if ($this->orderItemHasExtras->removeElement($orderItemHasExtra)) {
            // set the owning side to null (unless already changed)
            if ($orderItemHasExtra->getExtras() === $this) {
                $orderItemHasExtra->setExtras(null);
            }
        }

        return $this;
    }
}
