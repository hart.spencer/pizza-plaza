<?php

namespace App\Entity;

use App\Repository\PizzaHasExtraRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PizzaHasExtraRepository::class)
 */
class PizzaHasExtra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Pizzas::class, inversedBy="pizzaHasExtras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pizzas;

    /**
     * @ORM\ManyToOne(targetEntity=Extras::class, inversedBy="pizzaHasExtras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $extras;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPizzas(): ?Pizzas
    {
        return $this->pizzas;
    }

    public function setPizzas(?Pizzas $pizzas): self
    {
        $this->pizzas = $pizzas;

        return $this;
    }

    public function getExtras(): ?Extras
    {
        return $this->extras;
    }

    public function setExtras(?Extras $extras): self
    {
        $this->extras = $extras;

        return $this;
    }
}
