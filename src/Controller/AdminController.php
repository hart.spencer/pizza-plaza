<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Customer;
use App\Entity\Extras;
use App\Entity\Order;
use App\Entity\OrderItemHasExtra;
use App\Entity\OrderItems;
use App\Entity\PizzaHasExtra;
use App\Entity\Pizzas;

use App\Repository\CustomerRepository;
use App\Repository\ExtrasRepository;
use App\Repository\OrderRepository;
use App\Repository\OrderItemHasExtraRepository;
use App\Repository\OrderItemsRepository;
use App\Repository\PizzaHasExtraRepository;
use App\Repository\PizzasRepository;

use Illuminate\Support\Debug\Dumper;
use Doctrine\ORM\EntityManagerInterface;

class AdminController extends AbstractController
{

	public function __construct()
	{
	}

	public function index(CustomerRepository $customersRepo)
	{
		$customers = $customersRepo->findAll();

		return $this->render('security/admin.html.twig', [
			'menu' => 'admin',
			'customers' => $customers,
        ]);
	}

	public function getPizzaForToday($pizzasRepo){
		$pizzas= $pizzasRepo->findAll();
		$pizzas_id = array();

		foreach($pizzas as $pizza){
			array_push($pizzas_id, $pizza->getId());
		}

		srand(date("ymd"));
		$pizza_today = rand(min($pizzas_id), max($pizzas_id));

		$pizza = $pizzasRepo->find($pizza_today);

		return $pizza;
	}

	public function viewOrder(CustomerRepository $customersRepo, OrderRepository $orderRepo, PizzaHasExtraRepository $pizzaExtraRepo, ExtrasRepository $extrasRepo, OrderItemsRepository $orderItemRepo, OrderItemHasExtraRepository $orderItemHasExtraRepo,PizzasRepository $pizzasRepo, $id) : Response
	{
		$pizzaToday = $this->getPizzaForToday($pizzasRepo);

		$customer = $customersRepo->find($id);
		$orders = $orderRepo->findBy([
			'customer' => $customer->getId(),
		]);

		$orders_id_array = array();
		foreach($orders as $order){
			array_push($orders_id_array, $order->getId());
		}

		$order_items = $orderItemRepo->findBy(array('order' => $orders_id_array));
		$order_items_id_array = array();
		$pizzas_id = array();
		$extras_id = array();

		foreach($order_items as $order_item){
			array_push($order_items_id_array, $order_item->getId());
			array_push($pizzas_id, $order_item->getPizzas()->getId());
		}

		$pizzas = $pizzasRepo->findBy(array('id' => $pizzas_id));
		$order_item_has_extras = $orderItemHasExtraRepo->findBy(array('orderItems' => $order_items_id_array));

		foreach($order_item_has_extras as $order_item_has_extra){
			array_push($extras_id, $order_item_has_extra->getExtras());
		}

		$extras = $extrasRepo->findBy(array('id' => $extras_id));

		$pizzas_ = array();

		foreach($pizzas as $pizza){

			foreach($order_items as $order_item){
				$quantity_counter = 1;
				if($order_item->getPizzas($pizza)->getId() == $pizza->getId()){

					while ($quantity_counter <= $order_item->getQuantity()) {

						$pizzas_array = array();
						$pizzas_array = $pizzas_array + array('id' => $pizza->getId());
						$pizzas_array = $pizzas_array + array('name' => $pizza->getName());
						if($pizzaToday->getId() == $pizza->getId()){
							$pizzas_array = $pizzas_array + array('price' => round($pizza->getPrice() - $pizza->getPrice() * 0.33, 2));
						}else{
							$pizzas_array = $pizzas_array + array('price' => $pizza->getPrice());
						}
						$pizzas_array = $pizzas_array + array('description' => $pizza->getDescription());
						$id_pizza = $pizza->getId();
						$pizza_has_extra = $pizzaExtraRepo->findBy([
							'pizzas' => $id_pizza
						]);

						$extras_array_id = array();
						foreach($pizza_has_extra as $extras_pizza){
							array_push($extras_array_id, $extras_pizza->getExtras()->getId());
						}

						$extras_info = array();
						foreach ($extras as $extra_info) {
							if(in_array($extra_info->getId(), $extras_array_id)){
								$extra_info_array = array();
								$extra_info_array = $extra_info_array + array('id' => $extra_info->getId());
								$extra_info_array = $extra_info_array + array('name' => $extra_info->getName());
								$extra_info_array = $extra_info_array + array('price' => $extra_info->getPrice());
								array_push($extras_info, $extra_info_array);
							}
						}
						$pizzas_array = $pizzas_array + array('extras' => $extras_info);
						array_push($pizzas_, $pizzas_array);
						$quantity_counter++;
					}
				}

			}
		}

		return $this->render('security/view-order.html.twig', [
    		'menu' => 'admin',
    		'pizzas' => $pizzas_,
    		'customer' => $customer,
        ]);
	}

	public function deleteOrder(CustomerRepository $customersRepo, OrderRepository $orderRepo, PizzaHasExtraRepository $pizzaExtraRepo, ExtrasRepository $extrasRepo, OrderItemsRepository $orderItemRepo, OrderItemHasExtraRepository $orderItemHasExtraRepo,PizzasRepository $pizzasRepo, EntityManagerInterface $entityManager, $id)
	{
		$customer = $customersRepo->find($id);

		$orders = $orderRepo->findBy([
			'customer' => $customer->getId(),
		]);

		$orders_id_array = array();
		foreach($orders as $order){
			array_push($orders_id_array, $order->getId());
		}

		$order_items = $orderItemRepo->findBy(array('order' => $orders_id_array));
		$order_items_id_array = array();

		foreach($order_items as $order_item){
			array_push($order_items_id_array, $order_item->getId());
		}

		$order_item_has_extras = $orderItemHasExtraRepo->findBy(array('orderItems' => $order_items_id_array));
		$entityManager = $this->getDoctrine()->getManager();

	    $entityManager->remove($customer);
	    foreach($orders as $order){
	    	$entityManager->remove($order);
	    }
	    foreach($order_items as $order_item){
	    	$entityManager->remove($order_item);
	    }
	    foreach($order_item_has_extras as $order_item_has_extra){
	    	$entityManager->remove($order_item_has_extra);
	    }
	    $entityManager->flush();

        return $this->redirectToRoute('admin-dashboard');
	}
}

?>
