<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\HttpFoundation\Cookie;

use App\Entity\Customer;
use App\Entity\Extras;
use App\Entity\Order;
use App\Entity\OrderItemHasExtra;
use App\Entity\OrderItems;
use App\Entity\PizzaHasExtra;
use App\Entity\Pizzas;

use App\Repository\CustomerRepository;
use App\Repository\ExtrasRepository;
use App\Repository\OrderRepository;
use App\Repository\OrderItemHasExtraRepository;
use App\Repository\OrderItemsRepository;
use App\Repository\PizzaHasExtraRepository;
use App\Repository\PizzasRepository;

use Illuminate\Support\Debug\Dumper;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Yaml\Yaml;

class PizzaController extends AbstractController
{

	public function __construct(SessionInterface $session, ExtrasRepository $extrasRepo, PizzasRepository $pizzasRepo)
	{

		if($session->get('total_item') == null || $session->get('total_item') == 0 )
		{
			$session->set('order', []);
			$session->set('total_item', 0);
			$session->set('total_price', 0);

		}

		if($session->get('language') == null){
			$session->set('language', 'de');

		}


	}

    public function index(PizzasRepository $pizzasRepo,Request $request, SessionInterface $session)
    {
		$pizza_today = $this->getPizzaForToday($pizzasRepo);
		$user = $request->cookies->get('consent');

        $response = new Response();

        if (!$user){
			$show_consent = true;
            $cookie = new Cookie('consent', true, time() + (183 * 24 * 60 * 60));
            $response->headers->setCookie($cookie);
            $response->sendHeaders();
        }else{
			$show_consent = false;
		}

    	return $this->render('index.html.twig', [
    		'menu' => 'online-order',
            'extras' => false,
            'totalItem' => 0,
            'totalPrice' => 0,
			'consent' => $show_consent,
			'pizzaToday' => $pizza_today,
			'lang' => $session->get('language'),
        ], $response);
    }

	public function getPizzaForToday($pizzasRepo){

		$pizzas= $pizzasRepo->findAll();
		$pizzas_id = array();

		foreach($pizzas as $pizza){
			array_push($pizzas_id, $pizza->getId());
		}

		srand(date("ymd"));
		$pizza_today = rand(min($pizzas_id), max($pizzas_id));

		$pizza = $pizzasRepo->find($pizza_today);

		return $pizza;
	}

    public function getPizzasArray($pizzaExtraRepo, $pizzasRepo, $extrasRepo, $lang)
    {
		$pizzas = $pizzasRepo->findAll();
        $extras = $extrasRepo->findBy(
			['isChoosable' => '1']
		);

		$pizzas_ = array();

		foreach($pizzas as $pizza){

			$pizzas_array = array();
			$pizzas_array = $pizzas_array + array('id' => $pizza->getId());
			$pizzas_array = $pizzas_array + array('name' => $this->translateString($pizza->getName(), $lang));
			$pizzas_array = $pizzas_array + array('price' => $pizza->getPrice());
			$pizzas_array = $pizzas_array + array('description' => $this->translateString($pizza->getDescription(), $lang));

			$id_pizza = $pizza->getId();
			$pizza_has_extra = $pizzaExtraRepo->findBy([
				'pizzas' => $id_pizza
			]);

			// $extras_array = array();
			// foreach($pizza_has_extra as $extras_pizza){
			// 	array_push($extras_array, $extras_pizza->getId());
			// }

			$extras_array = array();
			foreach($pizza_has_extra as $extras_pizza){
				array_push($extras_array, $extras_pizza->getExtras()->getId());
			}

			$extras_ = $extrasRepo->findBy(array('id' => $extras_array));

			$extras_info = array();
			foreach ($extras_ as $extra_info) {
				if($extra_info->getIsChoosable() == 1){
					$extra_info_array = array();
					$extra_info_array = $extra_info_array + array('id' => $extra_info->getId());
					$extra_info_array = $extra_info_array + array('name' => $extra_info->getName());
					$extra_info_array = $extra_info_array + array('price' => $extra_info->getPrice());
					array_push($extras_info, $extra_info_array);
				}

			}

			$pizzas_array = $pizzas_array + array('extras' => $extras_info);
			array_push($pizzas_, $pizzas_array);
		}

		return $pizzas_;
    }

    public function showOnlineOrder(PizzaHasExtraRepository $pizzaExtraRepo, ExtrasRepository $extrasRepo, PizzasRepository $pizzasRepo, SessionInterface $session): Response
    {
		$order = $session->get('order');
		$lang = $session->get('language');
		$pizzaToday = $this->getPizzaForToday($pizzasRepo);
		$pizzas = $pizzasRepo->findAll();
        $extras = $extrasRepo->findBy(
			['isChoosable' => '1']
		);

		$pizzas_ = $this->getPizzasArray($pizzaExtraRepo, $pizzasRepo, $extrasRepo, $lang);

		dump($pizzas_);

    	return $this->render('online-order.html.twig', [
    		'menu' => 'online-ordering',
			'pizzas' => $pizzas_,
            'extras' => $extras,
            'totalItem' => $session->get('total_item'),
            'totalPrice' => $session->get('total_price'),
			'pizzaToday' => $pizzaToday,
			'lang' => $session->get('language'),
        ]);
    }

	public function addOrder(ExtrasRepository $extrasRepo, PizzasRepository $pizzasRepo, Request $request, SessionInterface $session, $id)
	{
		$session->set('total_item', $session->get('total_item') + 1);
		$session->set('total_price', $request->request->get('total_price'));

		$pizzas = $pizzasRepo->findBy(
					['name' => $request->request->get('pizza_type')]
				);
		$extras = $extrasRepo->findBy(
					['isChoosable' => '1']
				);

		$order = $session->get('order');

		foreach ($pizzas as $key => $pizza) {

			$order_info = array();
			$order_info = $order_info + array('id' => $pizza->getId());
			$order_info = $order_info + array('name' => $pizza->getName());
			$order_info = $order_info + array('price' => $pizza->getPrice());

			$extras_array = array();
			foreach ($extras as $extra)
			{
				if($request->request->get($extra->getName()))
				{
					$extra_info = array();
					$extra_info = $extra_info + array('id' => $extra->getId());
					$extra_info = $extra_info + array('name' => $extra->getName());
					$extra_info = $extra_info + array('price' => $extra->getPrice());
					array_push($extras_array, $extra_info);
				}
			}

			$order_info = $order_info + array('extras' => $extras_array);
			array_push($order, $order_info);
		}
		dump($order);

		$session->set('order', $order);

		return new Response(
			$session->get('total_item'),
			Response::HTTP_OK
		);
	}

	public function showCheckOut(PizzaHasExtraRepository $pizzaExtraRepo, ExtrasRepository $extrasRepo, PizzasRepository $pizzasRepo, SessionInterface $session): Response
	{
		$lang = $session->get('language');
		$pizzaToday = $this->getPizzaForToday($pizzasRepo);

		$pizzas = $pizzasRepo->findAll();
		$extras = $extrasRepo->findBy(
					['isChoosable' => '1']
				);
		$order = $session->get('order');

		$pizzas_ = $this->getPizzasArray($pizzaExtraRepo, $pizzasRepo, $extrasRepo, $lang);

		dump(array_search('Tricolore' ,array_column($pizzas_, 'name')));

		return $this->render('checkout-order.html.twig', [
			'menu' => '',
			'pizzas' => $pizzas_,
			'extras' => $extras,
			'order' => $order,
			'totalItem' => $session->get('total_item'),
			'totalPrice' => $session->get('total_price'),
			'pizzaToday' => $pizzaToday,
			'lang' => $session->get('language'),
		]);
	}

	public function removePizza(SessionInterface $session, Request $request, $id)
	{
		$message = 'default';
		$order = $session->get('order');

		unset($order[$id]);

		$session->set('order', $order);
		$session->set('total_item', $session->get('total_item') - 1);

		return new Response(
			json_encode($order),
			Response::HTTP_OK
		);
	}

	public function removeExtra(SessionInterface $session, Request $request, $id)
	{
		$order = $session->get('order');
		$totalPrice = $session->get('total_price');

		$session->set('total_price', $totalPrice - $order[$id]['extras'][$request->request->get('extra_key')]['price']);

		unset($order[$id]['extras'][$request->request->get('extra_key')]);

		$session->set('order', $order);

		return new Response(
			json_encode($order),
			Response::HTTP_OK
		);
	}

	public function addExtras(ExtrasRepository $extrasRepo, PizzasRepository $pizzasRepo, SessionInterface $session, Request $request, $id)
	{
		$order = $session->get('order');
		$totalPrice = $session->get('total_price');

		$pizzas = $pizzasRepo->findBy(
					['name' => $request->request->get('pizza_type')]
				);
		$extras = $extrasRepo->findBy(
					['isChoosable' => '1']
				);

		$old_extras_price = 0;
		foreach($order[$id]['extras'] as $extra){
			$old_extras_price = $old_extras_price + $extra['price'];
		}

		$totalPrice = $totalPrice - $old_extras_price;

		unset($order[$id]['extras']);

		$extras_array = array();
		$new_extras_price = 0;
		foreach ($extras as $extra)
		{
			if($request->request->get($extra->getName()))
			{
				$extra_info = array();
				$extra_info = $extra_info + array('id' => $extra->getId());
				$extra_info = $extra_info + array('name' => $extra->getName());
				$extra_info = $extra_info + array('price' => $extra->getPrice());
				array_push($extras_array, $extra_info);
				$new_extras_price = $new_extras_price + $extra->getPrice();

			}
		}

		$totalPrice = $totalPrice + $new_extras_price;

		$order[$id] = $order[$id] + array('extras' => $extras_array);

		$session->set('order', $order);
		$session->set('total_price', $totalPrice);
		return new Response(
			'extras-added',
			Response::HTTP_OK
		);
	}

	public function submitOrder(PizzaHasExtraRepository $pizzaExtraRepo,ExtrasRepository $extrasRepo, PizzasRepository $pizzasRepo, SessionInterface $session, Request $request, EntityManagerInterface $entityManager): Response
	{
		$order = $session->get('order');
		$id_array = array_column($order, 'id');
		array_multisort($id_array, SORT_DESC, $order);


		$entityManager = $this->getDoctrine()->getManager();

		$customer_entity = new Customer();
		$customer_entity->setFirstname($request->request->get('firstName'));
		$customer_entity->setLastname($request->request->get('lastName'));
		$customer_entity->setCity($request->request->get('city'));
		$customer_entity->setStreet($request->request->get('street'));
		$customer_entity->setStreetnumber( $request->request->get('streetNo'));
		$customer_entity->setZip($request->request->get('zip'));
		$customer_entity->setPhone($request->request->get('phone'));
		$entityManager->persist($customer_entity);

		$order_entity = new Order();
		$order_entity->setTimeStamp(new \DateTime());
		$order_entity->setCustomer($customer_entity);
		$entityManager->persist($order_entity);

		$pizza_id = 0;
		$pizza_quantity = 1;
		$insert_order_items = FALSE;
		$next_pizza = FALSE;
		$cnt = 1;

		foreach ($order as $key => $order_info) {

			if($next_pizza == FALSE){
				$pizza = $pizzasRepo->find($order_info['id']);
				$pizza_id = $order_info['id'];
				$pizza_extras = $order_info['extras'];
			}

			// Same Pizza from previous Pizza
			if($next_pizza == TRUE){

				$same_extras = TRUE;
				foreach ($order_info['extras'] as $extras) {

					if(!in_array($extras['name'], array_column($pizza_extras, 'name'))){
						$same_extras = FALSE;
					}
				}

				if($same_extras){
					$pizza_quantity++;
				}else{
					// commit previous pizza to orderitem
					${"order_items" . $key-1} = new OrderItems();
					${"order_items" . $key-1}->setQuantity($pizza_quantity);
					${"order_items" . $key-1}->setPizzas($pizza);
					${"order_items" . $key-1}->setOrder($order_entity);
					$entityManager->persist(${"order_items" . $key-1});

					if(!empty($pizza_extras)){
						foreach($pizza_extras as $order_extra_key => $order_extra){

							// save to OrderItemHasExtra if orderitem has extra
							${"order_item_extra" . $order_extra_key-1} = new OrderItemHasExtra();
							${"order_item_extra" . $order_extra_key-1}->setOrderItems(${"order_items" . $key-1});
							${"order_item_extra" . $order_extra_key-1}->setExtras($extrasRepo->find($order_extra['id']));
							$entityManager->persist(${"order_item_extra" . $order_extra_key-1});

						}
					}

					//create new orderitem for pizza entry
					$pizza = $pizzasRepo->find($order_info['id']);
					$pizza_id = $order_info['id'];
					$pizza_quantity = 1;
					$pizza_extras = $order_info['extras'];
				}
			}

			// last iteration
			if ($key === array_key_last($order)){
				$insert_order_items = TRUE;

			}else{
				if($order[$key + 1]['id'] == $pizza_id){
					$next_pizza = TRUE;
				}else{
					$next_pizza = FALSE;
					$insert_order_items = TRUE;

				}
			}

			if($insert_order_items){

				${"order_items" . $key} = new OrderItems();
				${"order_items" . $key}->setQuantity($pizza_quantity);
				${"order_items" . $key}->setPizzas($pizza);
				${"order_items" . $key}->setOrder($order_entity);
				$entityManager->persist(${"order_items" . $key});

				if(!empty($pizza_extras)){
					foreach($pizza_extras as $order_extra_key => $order_extra){

						// save to OrderItemHasExtra if orderitem has extra
						${"order_item_extra" . $order_extra_key} = new OrderItemHasExtra();
						${"order_item_extra" . $order_extra_key}->setOrderItems(${"order_items" . $key});
						${"order_item_extra" . $order_extra_key}->setExtras($extrasRepo->find($order_extra['id']));
						$entityManager->persist(${"order_item_extra" . $order_extra_key});

					}
				}
			}
			$insert_order_items = FALSE;
			$cnt++;
		}

		// dump($cnt);exit(0);

        $entityManager->flush();

        $session->remove('total_item');
        $session->remove('order');

        return $this->redirectToRoute('online-ordering');
	}

	public function Contact(SessionInterface $session) : Response
	{

		return $this->render('contact.html.twig', [
    		'menu' => 'contact',
			'pizzas' => '',
            'extras' => '',
            'totalItem' => '',
            'totalPrice' => '',
			'pizzaToday' => '',
			'lang' => $session->get('language'),
        ]);
	}

	public function About(SessionInterface $session) : Response
	{

		return $this->render('about.html.twig', [
    		'menu' => 'about',
			'pizzas' => '',
            'extras' => '',
            'totalItem' => '',
            'totalPrice' => '',
			'pizzaToday' => '',
			'lang' => $session->get('language'),
        ]);
	}

	public function Imprint(SessionInterface $session) : Response
	{

		return $this->render('imprint.html.twig', [
    		'menu' => 'imprint',
			'pizzas' => '',
            'extras' => '',
            'totalItem' => '',
            'totalPrice' => '',
			'pizzaToday' => '',
			'lang' => $session->get('language'),
        ]);
	}

	public function setLang(SessionInterface $session, Request $request)
	{


		$session->set('language', $request->request->get('lang'));

		return new Response(
			$session->get('language'),
			Response::HTTP_OK
		);
	}

	public function translateString($string_desc, $lang)
	{
		if($lang == 'en'){

			$string = strstr($string_desc, '.,-%!');

			$string_desc_array = explode(" ", $string_desc);

			$translated_desc = array();

			$en_lang = Yaml::parseFile(\dirname(__DIR__).'/language.yaml');

			foreach($string_desc_array as $string_word){
				if(array_key_exists($string_word, $en_lang)){
					array_push($translated_desc, $en_lang[$string_word]);
				}else{
					array_push($translated_desc, $string_word);
				}
			}
			$string_desc = implode(" ", $translated_desc);
		}

		return $string_desc;
	}
}

?>
