<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210616034301 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F5299398B171EB6C');
        $this->addSql('DROP INDEX IDX_F5299398B171EB6C ON `order`');
        $this->addSql('ALTER TABLE `order` CHANGE customer_id_id customer_id INT NOT NULL');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993989395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_F52993989395C3F3 ON `order` (customer_id)');
        $this->addSql('ALTER TABLE order_item_has_extra DROP FOREIGN KEY FK_D21CDB9B2B4D198B');
        $this->addSql('ALTER TABLE order_item_has_extra DROP FOREIGN KEY FK_D21CDB9B602A2B6A');
        $this->addSql('DROP INDEX IDX_D21CDB9B602A2B6A ON order_item_has_extra');
        $this->addSql('DROP INDEX IDX_D21CDB9B2B4D198B ON order_item_has_extra');
        $this->addSql('ALTER TABLE order_item_has_extra ADD order_items_id INT NOT NULL, ADD extras_id INT NOT NULL, DROP order_items_id_id, DROP extras_id_id');
        $this->addSql('ALTER TABLE order_item_has_extra ADD CONSTRAINT FK_D21CDB9B8A484C35 FOREIGN KEY (order_items_id) REFERENCES order_items (id)');
        $this->addSql('ALTER TABLE order_item_has_extra ADD CONSTRAINT FK_D21CDB9B955D4F3F FOREIGN KEY (extras_id) REFERENCES extras (id)');
        $this->addSql('CREATE INDEX IDX_D21CDB9B8A484C35 ON order_item_has_extra (order_items_id)');
        $this->addSql('CREATE INDEX IDX_D21CDB9B955D4F3F ON order_item_has_extra (extras_id)');
        $this->addSql('ALTER TABLE order_items DROP FOREIGN KEY FK_62809DB0759F0AC7');
        $this->addSql('ALTER TABLE order_items DROP FOREIGN KEY FK_62809DB0FCDAEAAA');
        $this->addSql('DROP INDEX IDX_62809DB0FCDAEAAA ON order_items');
        $this->addSql('DROP INDEX IDX_62809DB0759F0AC7 ON order_items');
        $this->addSql('ALTER TABLE order_items ADD order_id INT NOT NULL, ADD pizzas_id INT NOT NULL, DROP order_id_id, DROP pizzas_id_id');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_62809DB08D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_62809DB07F778084 FOREIGN KEY (pizzas_id) REFERENCES pizzas (id)');
        $this->addSql('CREATE INDEX IDX_62809DB08D9F6D38 ON order_items (order_id)');
        $this->addSql('CREATE INDEX IDX_62809DB07F778084 ON order_items (pizzas_id)');
        $this->addSql('ALTER TABLE pizza_has_extra DROP FOREIGN KEY FK_3428475D2B4D198B');
        $this->addSql('ALTER TABLE pizza_has_extra DROP FOREIGN KEY FK_3428475D759F0AC7');
        $this->addSql('DROP INDEX IDX_3428475D759F0AC7 ON pizza_has_extra');
        $this->addSql('DROP INDEX IDX_3428475D2B4D198B ON pizza_has_extra');
        $this->addSql('ALTER TABLE pizza_has_extra ADD pizzas_id INT NOT NULL, ADD extras_id INT NOT NULL, DROP pizzas_id_id, DROP extras_id_id');
        $this->addSql('ALTER TABLE pizza_has_extra ADD CONSTRAINT FK_3428475D7F778084 FOREIGN KEY (pizzas_id) REFERENCES pizzas (id)');
        $this->addSql('ALTER TABLE pizza_has_extra ADD CONSTRAINT FK_3428475D955D4F3F FOREIGN KEY (extras_id) REFERENCES extras (id)');
        $this->addSql('CREATE INDEX IDX_3428475D7F778084 ON pizza_has_extra (pizzas_id)');
        $this->addSql('CREATE INDEX IDX_3428475D955D4F3F ON pizza_has_extra (extras_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F52993989395C3F3');
        $this->addSql('DROP INDEX IDX_F52993989395C3F3 ON `order`');
        $this->addSql('ALTER TABLE `order` CHANGE customer_id customer_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398B171EB6C FOREIGN KEY (customer_id_id) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_F5299398B171EB6C ON `order` (customer_id_id)');
        $this->addSql('ALTER TABLE order_item_has_extra DROP FOREIGN KEY FK_D21CDB9B8A484C35');
        $this->addSql('ALTER TABLE order_item_has_extra DROP FOREIGN KEY FK_D21CDB9B955D4F3F');
        $this->addSql('DROP INDEX IDX_D21CDB9B8A484C35 ON order_item_has_extra');
        $this->addSql('DROP INDEX IDX_D21CDB9B955D4F3F ON order_item_has_extra');
        $this->addSql('ALTER TABLE order_item_has_extra ADD order_items_id_id INT NOT NULL, ADD extras_id_id INT NOT NULL, DROP order_items_id, DROP extras_id');
        $this->addSql('ALTER TABLE order_item_has_extra ADD CONSTRAINT FK_D21CDB9B2B4D198B FOREIGN KEY (extras_id_id) REFERENCES extras (id)');
        $this->addSql('ALTER TABLE order_item_has_extra ADD CONSTRAINT FK_D21CDB9B602A2B6A FOREIGN KEY (order_items_id_id) REFERENCES order_items (id)');
        $this->addSql('CREATE INDEX IDX_D21CDB9B602A2B6A ON order_item_has_extra (order_items_id_id)');
        $this->addSql('CREATE INDEX IDX_D21CDB9B2B4D198B ON order_item_has_extra (extras_id_id)');
        $this->addSql('ALTER TABLE order_items DROP FOREIGN KEY FK_62809DB08D9F6D38');
        $this->addSql('ALTER TABLE order_items DROP FOREIGN KEY FK_62809DB07F778084');
        $this->addSql('DROP INDEX IDX_62809DB08D9F6D38 ON order_items');
        $this->addSql('DROP INDEX IDX_62809DB07F778084 ON order_items');
        $this->addSql('ALTER TABLE order_items ADD order_id_id INT NOT NULL, ADD pizzas_id_id INT NOT NULL, DROP order_id, DROP pizzas_id');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_62809DB0759F0AC7 FOREIGN KEY (pizzas_id_id) REFERENCES pizzas (id)');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_62809DB0FCDAEAAA FOREIGN KEY (order_id_id) REFERENCES `order` (id)');
        $this->addSql('CREATE INDEX IDX_62809DB0FCDAEAAA ON order_items (order_id_id)');
        $this->addSql('CREATE INDEX IDX_62809DB0759F0AC7 ON order_items (pizzas_id_id)');
        $this->addSql('ALTER TABLE pizza_has_extra DROP FOREIGN KEY FK_3428475D7F778084');
        $this->addSql('ALTER TABLE pizza_has_extra DROP FOREIGN KEY FK_3428475D955D4F3F');
        $this->addSql('DROP INDEX IDX_3428475D7F778084 ON pizza_has_extra');
        $this->addSql('DROP INDEX IDX_3428475D955D4F3F ON pizza_has_extra');
        $this->addSql('ALTER TABLE pizza_has_extra ADD pizzas_id_id INT NOT NULL, ADD extras_id_id INT NOT NULL, DROP pizzas_id, DROP extras_id');
        $this->addSql('ALTER TABLE pizza_has_extra ADD CONSTRAINT FK_3428475D2B4D198B FOREIGN KEY (extras_id_id) REFERENCES extras (id)');
        $this->addSql('ALTER TABLE pizza_has_extra ADD CONSTRAINT FK_3428475D759F0AC7 FOREIGN KEY (pizzas_id_id) REFERENCES pizzas (id)');
        $this->addSql('CREATE INDEX IDX_3428475D759F0AC7 ON pizza_has_extra (pizzas_id_id)');
        $this->addSql('CREATE INDEX IDX_3428475D2B4D198B ON pizza_has_extra (extras_id_id)');
    }
}
