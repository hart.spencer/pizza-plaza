<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210616031110 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE extras (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, is_choosable TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, customer_id_id INT NOT NULL, timestamp DATETIME NOT NULL, INDEX IDX_F5299398B171EB6C (customer_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_item_has_extra (id INT AUTO_INCREMENT NOT NULL, order_items_id_id INT NOT NULL, extras_id_id INT NOT NULL, INDEX IDX_D21CDB9B602A2B6A (order_items_id_id), INDEX IDX_D21CDB9B2B4D198B (extras_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_items (id INT AUTO_INCREMENT NOT NULL, order_id_id INT NOT NULL, pizzas_id_id INT NOT NULL, quantity INT NOT NULL, INDEX IDX_62809DB0FCDAEAAA (order_id_id), INDEX IDX_62809DB0759F0AC7 (pizzas_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pizza_has_extra (id INT AUTO_INCREMENT NOT NULL, pizzas_id_id INT NOT NULL, extras_id_id INT NOT NULL, INDEX IDX_3428475D759F0AC7 (pizzas_id_id), INDEX IDX_3428475D2B4D198B (extras_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pizzas (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398B171EB6C FOREIGN KEY (customer_id_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE order_item_has_extra ADD CONSTRAINT FK_D21CDB9B602A2B6A FOREIGN KEY (order_items_id_id) REFERENCES order_items (id)');
        $this->addSql('ALTER TABLE order_item_has_extra ADD CONSTRAINT FK_D21CDB9B2B4D198B FOREIGN KEY (extras_id_id) REFERENCES extras (id)');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_62809DB0FCDAEAAA FOREIGN KEY (order_id_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_62809DB0759F0AC7 FOREIGN KEY (pizzas_id_id) REFERENCES pizzas (id)');
        $this->addSql('ALTER TABLE pizza_has_extra ADD CONSTRAINT FK_3428475D759F0AC7 FOREIGN KEY (pizzas_id_id) REFERENCES pizzas (id)');
        $this->addSql('ALTER TABLE pizza_has_extra ADD CONSTRAINT FK_3428475D2B4D198B FOREIGN KEY (extras_id_id) REFERENCES extras (id)');
        $this->addSql('ALTER TABLE customer CHANGE phone phone VARCHAR(15) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_item_has_extra DROP FOREIGN KEY FK_D21CDB9B2B4D198B');
        $this->addSql('ALTER TABLE pizza_has_extra DROP FOREIGN KEY FK_3428475D2B4D198B');
        $this->addSql('ALTER TABLE order_items DROP FOREIGN KEY FK_62809DB0FCDAEAAA');
        $this->addSql('ALTER TABLE order_item_has_extra DROP FOREIGN KEY FK_D21CDB9B602A2B6A');
        $this->addSql('ALTER TABLE order_items DROP FOREIGN KEY FK_62809DB0759F0AC7');
        $this->addSql('ALTER TABLE pizza_has_extra DROP FOREIGN KEY FK_3428475D759F0AC7');
        $this->addSql('DROP TABLE extras');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE order_item_has_extra');
        $this->addSql('DROP TABLE order_items');
        $this->addSql('DROP TABLE pizza_has_extra');
        $this->addSql('DROP TABLE pizzas');
        $this->addSql('ALTER TABLE customer CHANGE phone phone VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
